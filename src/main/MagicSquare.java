package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;


class IndividuoSquare implements Comparable<IndividuoSquare> {
	int n, n_sq;
	public int[][] square;
	
	public IndividuoSquare(int n) {
		this.n = n;
		this.n_sq = n * n;
		this.square = new int[n][n];
//		generateRandom();
	}
	
//	public IndividuoSquare(int n, int[][] square) {
//		this.n = n;
//		this.n_sq = n * n;
//		this.square = square.clone();
//	}
	

	private void swap(int i, int j, int a, int b){
		int tmp = square[i][j];
		square[i][j] = square[a][b];
		square[a][b] = tmp;
	}
	
	public void generateRandom(){
		for(int i = 0; i < n_sq; i++) {
			square[i/n][i%n] = i + 1;
		}
		
		for(int i = 0; i < n_sq; i++) {
			swap(i/n, i%n, MagicSquare.sqRand.nextInt(n), MagicSquare.sqRand.nextInt(n));
		}
		
		//square = new int[][]{{2,7,6}, {9,5,1}, {4,3,8}};
		
		System.out.println(this.toString());
		System.out.println("Fitness: " + getFitness());
	}
	
	public boolean isMagic(){
		return getFitness() == 0;
	}
	
	public int getSumRow(int row){
		int sum = 0;
		for(int i = 0; i < n; i++) sum += square[row][i];
		return sum;
	}
	
	public int getSumDiag1(){
		int sum = 0;
		for(int i = 0; i < n; i++) sum += square[i][i];
		return sum;
	}
	
	public int getSumDiag2(){
		int sum = 0;
		for(int i = 0; i < n; i++) sum += square[i][n-i-1];
		return sum;
	}
	
	public int getSumCol(int col){
		int sum = 0;
		for(int i = 0; i < n; i++) sum += square[i][col];
		return sum;
	}
	
	public int getFitness(){
		int magicNumber = MagicSquare.getMagicNumber(n);
		int fitness = 0;
		
		fitness += Math.pow((getSumDiag1() - magicNumber), 2);
		fitness += Math.pow((getSumDiag2() - magicNumber), 2);
		for(int i = 0; i < n; i++){
			fitness += Math.pow((getSumRow(i) - magicNumber), 2);
			fitness += Math.pow((getSumCol(i) - magicNumber), 2);
		}
	
		return fitness;
	}
	
	public void mutate(double rate){
		for(int i = 0; i < n_sq; i++) {
			if(MagicSquare.sqRand.nextDouble() < rate){
				swap(i/n, i%n, MagicSquare.sqRand.nextInt(n), MagicSquare.sqRand.nextInt(n));
			}
		}
	}
	
	public void cross(int irow, int[] row){
		square[irow] = row.clone();
	
		Arrays.fill(MagicSquare.visitedCache, 0);
		for(int i = 0; i < n; i++) MagicSquare.visitedCache[row[i]-1] = 1;
		
		for(int i = 0; i < n; i++){
			if(i != irow){
				for(int j = 0; j < n; j++){
					int a = square[i][j]-1;
					while(MagicSquare.visitedCache[a] == 1){
						a = (a + 1) % n_sq;
//						System.out.println(a);
					}
					square[i][j] = a+1;
					MagicSquare.visitedCache[a] = 1;
				}
			}
		}
		if(!isValid()) System.out.println("INVALID!");
	}
	
	public boolean isValid(){
		int[] check = new int[n_sq];
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				if(check[square[i][j]-1] == 1) return false;
				check[square[i][j]-1]++;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		String result = "[";
		for (int i = 0; i < n; i++) {
			result += Arrays.toString(square[i]);
		}
		return result + "]";
	}

	@Override
	public int compareTo(IndividuoSquare o) {
		int myFitness = getFitness();
		int otherFitness = o.getFitness();
		
		if(myFitness > otherFitness) return 1;
		else if(myFitness < otherFitness) return -1;
		return 0;
	}	
	
	@Override
	protected IndividuoSquare clone() {
		IndividuoSquare a = new IndividuoSquare(n);
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				a.square[i][j] = square[i][j];
			}
		}
		return a;
	}
}

public class MagicSquare {
	public static int[] visitedCache;
	public static Random sqRand = new Random(54878);
	
	//TODO: Colocar no construtor
	int populationSize = 1000;
	double crossRate = 0.6;
	double mutationRate = 0.1;
	int n;
	
	private List<IndividuoSquare> population;
	
	public static int getMagicNumber(int n){
		return (int)(0.5 * n * (n * n + 1));
	}

	public MagicSquare(int n) {
		this.n = n;
		visitedCache = new int[n*n + 1];
		population = new ArrayList<IndividuoSquare>(populationSize);
		makeRandomPopulation();
	}
	
	public void makeRandomPopulation(){
		population.clear();
		for(int i = 0; i < populationSize; i++){
			IndividuoSquare a = new IndividuoSquare(n);
			a.generateRandom();
			population.add(a);
		}
	}
	
	public void run(){
		int bestFitness = Integer.MAX_VALUE;
		
		int i;
		for(i = 0; i < 1000; i++){
			//if(i%1000==0)makeRandomPopulation();
			
			if(i%100==0) System.out.println("Geração: " + i);
			
			cross();
//			for(int j = 0; j < n; j++) population.get(j).mutate(0.5);
			Collections.sort(population);
//			population = population.subList(0, populationSize);

			int tpop;
			while((tpop = population.size()) > populationSize) population.remove(tpop-1);
			
		
			int partialFitness = population.get(0).getFitness();
			bestFitness = Math.min(bestFitness, partialFitness);
			if(i%100==0) System.out.println("Menor Fitness: " + bestFitness);
			
			if(bestFitness == 0) break;
		}
		
		System.out.println("Found fitness on epoch: " + i);
		System.out.println("Quadrado: " + population.get(0));
	}
	
	public void cross(){
		int populationSize = population.size();
		
		for (int i = 0; i < populationSize ; i++) {			
			if(sqRand.nextDouble() < crossRate){
				IndividuoSquare parentA = population.get(i);
				IndividuoSquare parentB = population.get(sqRand.nextInt((int)(populationSize * 0.2)));
				
				IndividuoSquare childA = parentA.clone();
				IndividuoSquare childB = parentB.clone();
				
				int la = sqRand.nextInt(n);
				int lb = sqRand.nextInt(n);
				if(lb == la) lb = (lb + 1) % n;

				
//				childA.cross(la, parentB.square[lb]);	
//				childB.cross(lb, parentA.square[la]);
				
				IndividuoSquare childC = childA.clone();
				IndividuoSquare childD = childB.clone();
				
				childC.mutate(0.1);
				childD.mutate(0.1);
				
				population.add(childA);
				population.add(childB);
				population.add(childC);
				population.add(childD);
			}
		}
	}
	

	public void select(){
		
	}
}
