package main;


public class IndividuoDouble extends Individuo<Double> {	
	public IndividuoDouble(int numDimensions, double minLimit, double maxLimit){
		super(numDimensions, minLimit, maxLimit);		
		chromossome = new Double[this.numDimensions];
		
		fillChromossome();
	}
	
	public IndividuoDouble(int numDimensions, double minLimit, double maxLimit, Double[] data){
		super(numDimensions, minLimit, maxLimit, data);	
	}

	protected void fillChromossome(){
		for(int i = 0; i < chromossome.length; i++){
			chromossome[i] = getNextDoubleRand();
		}
	}
	
	private double getNextDoubleRand(){
		return indRand.nextDouble() * (maxLimit-minLimit) + minLimit;
	}
	
	public void mutateGene(int a){
		chromossome[a] += indRand.nextDouble() * 5.0 + -2.5;
		if(chromossome[a] > maxLimit) chromossome[a] = maxLimit;
		if(chromossome[a] < minLimit) chromossome[a] = minLimit;
	}
	
	@Override
	public Double[] toDouble() {
		return chromossome;
	}
}
