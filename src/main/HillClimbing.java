package main;

import java.util.Random;

public class HillClimbing {
	Random customRand = new Random(1000);
	double probNoise = 0.05;
	int numDimensions = 100;
	double maxLimit = 100;
	double minLimit = -100;
	
	FitnessFunction function = new ShiftedSphere();
	Double[] base;
	
	public HillClimbing() {
		 base = createRandom();
	}
	
	private Double[] createRandom(){
		Double[] d = new Double[numDimensions];
		for(int i = 0; i < d.length; i++) d[i] = random();
		return d;
	}
	
	double random(){
		return customRand.nextDouble() * (maxLimit-minLimit) + minLimit;
	}
	
	
	double random(double min, double max){
		return customRand.nextDouble() * (max-min) + min;
	}
	
	Double[] tweak(Double[] d){
		for(int i = 0; i < d.length; i++){
			if(probNoise >= customRand.nextDouble()){
				double x = random() + d[i];
				if(x > maxLimit) x = maxLimit;
				if(x < minLimit) x = minLimit;
				d[i] = x;
			}
		}
		return d;
	}
	
	Double[] getPOGGradient(Double[] d){
		Double[] gradient = new Double[d.length];
		
		for(int i = 0; i < d.length; i++){
			Double[] copy = d.clone();
			copy[i] += 0.01;
			if(function.getFitness(copy, base) < function.getFitness(d, base)) gradient[i] = +1.0;
			else gradient[i] = -1.0;
		}
		
		return gradient;
	}
	
	Double[] tweak2(Double[] d){
		Double[] gradient = getPOGGradient(d);
		for(int i = 0; i < d.length; i++){
			if(probNoise >= customRand.nextDouble()){
				double x = d[i] + gradient[i] * random(0.0, 5.0);
				if(x > maxLimit) x = maxLimit;
				if(x < minLimit) x = minLimit;
				d[i] = x;
			}
		}
		return d;
	}
	
	void climb(){
		Double[] S = createRandom();
		Double[] best = S;
		
		for(int i = 0; i < 10; i++){
			// TODO: Make random restart the right way, idiot!
			int time = 10000;
			
			for(int j = 0; j < time; j++){
				Double[] R = tweak2(S.clone());
				if(function.getFitness(R, base) < function.getFitness(S, base)) S = R;
			}
			
			if(function.getFitness(S, base) < function.getFitness(best, base)) best = S;
			
			S = createRandom();
		}
		
		System.out.println("Menor fitness: " + function.getFitness(best, base));
	}
}
