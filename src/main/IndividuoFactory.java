package main;

public interface IndividuoFactory {
	Individuo<?> createIndividuo(int numDimensions, double minLimit, double maxLimit);
	Individuo<?> createIndividuo(int numDimensions, double minLimit, double maxLimit, Object[] data);
}

class IndividuoDoubleFactory implements IndividuoFactory {
	@Override
	public Individuo<?> createIndividuo(int numDimensions, double minLimit, double maxLimit) {
		return new IndividuoDouble(numDimensions, minLimit, maxLimit);
	}
	
	@Override
	public Individuo<?> createIndividuo(int numDimensions, double minLimit, double maxLimit, Object[] data){
		return new IndividuoDouble(numDimensions, minLimit, maxLimit, (Double[]) data);
	}
}


class IndividuoBinaryFactory implements IndividuoFactory {
	@Override
	public Individuo<?> createIndividuo(int numDimensions, double minLimit, double maxLimit) {
		return new IndividuoBinary(numDimensions, minLimit, maxLimit);
	}
	
	@Override
	public Individuo<?> createIndividuo(int numDimensions, double minLimit, double maxLimit, Object[] data){
		return new IndividuoBinary(numDimensions, minLimit, maxLimit, (String[]) data);
	}
}
