package main;

class ShiftedSphere implements FitnessFunction {
	public double getFitness(Double[] x, Double[] o){
		double value = -450.0;
		int D = x.length;
		
		for(int i = 0; i < D; i++){
			value += (x[i] - o[i]) * (x[i] - o[i]);
		}
		
		return value;
	}

}


class ShiftedRastrigin implements FitnessFunction {
	@Override
	public double getFitness(Double[] x, Double[] o) {
		double value = -330.0;
		int D = x.length;
		
		value += 10*D;
		for(int i = 0; i < D; i++){
			double diff = x[i] - o[i];
			value += (diff*diff) - 10*Math.cos(2*Math.PI*diff);
		}
		
		return value;
	}
}
