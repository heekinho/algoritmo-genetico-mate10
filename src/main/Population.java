package main;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

@SuppressWarnings({"rawtypes", "unchecked"})
public class Population {
	private Random matingRand = new Random(6457);
	
	private Individuo chromosomeFitnessBase;
	
	private ArrayList<Individuo> population;
	private double totalFitness;
	
	private int maxPopulation;
	private int numDimensions;
	private double minLimit;
	private double maxLimit;
	private FitnessFunction fitnessFunction;
	
	IndividuoFactory factory;

	private double crossRate;
	private double mutationRate;

	public Population(int maxPopulation, int numDimensions, double minLimit, double maxLimit, FitnessFunction fitnessFunction,
			double crossRate, double mutationRate, IndividuoFactory factory) {
		this.numDimensions = numDimensions;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
		this.fitnessFunction = fitnessFunction;
		this.maxPopulation = maxPopulation;
		
		this.crossRate = crossRate;
		this.mutationRate = mutationRate;
		this.factory = factory;
		
		chromosomeFitnessBase = factory.createIndividuo(numDimensions, minLimit, maxLimit);//new IndividuoBinary(numDimensions, minLimit, maxLimit);
		chromosomeFitnessBase.setFitness(calculateFitness(chromosomeFitnessBase));
		System.out.println("BASE: " + chromosomeFitnessBase);
		
		population = new ArrayList<>(maxPopulation);
		for(int i = 0; i < maxPopulation; i++){
			population.add(factory.createIndividuo(numDimensions, minLimit, maxLimit));//new IndividuoBinary(numDimensions, minLimit, maxLimit));
			System.out.println(population.get(i));
		}
	}
	
	public double calculateFitness(Individuo individuo){
		return fitnessFunction.getFitness(individuo.toDouble(), chromosomeFitnessBase.toDouble());
	}
	
	public void calculateFitnesses(){
		totalFitness = 0;
		for(Individuo individuo : population){
			double fitness = calculateFitness(individuo);
			individuo.setFitness(fitness);
			//System.out.println("Fitness: " + fitness);
			
			totalFitness += fitness;
		}
		System.out.println("Total Fitness: " + totalFitness);
	}
	
	public void evolve(){
//		// Calcula o fitness de geral
//		calculateFitnesses();
//		
	
		//boolean flag = false;
		for (int t = 0; t < 1000; t++) {
			System.out.println("Generation: " + t);
			calculateFitnesses();
			doMating();
			doNaturalSelection();
		}
		calculateFitnesses();
		Collections.sort(population);
		for(int i = 0; i < population.size(); i++){
			System.out.println(i + " : " + population.get(i).getFitness());
		}
		
	}
	
	
	/**
	 * TODO: Escolher método de seleção natural
	 */
	public void doNaturalSelection(){	
		elitist();
	}
	
	
	public void elitist(){
		Collections.sort(population);
		ArrayList<Individuo> newpopulation = new ArrayList<Individuo>(maxPopulation);
		for (int i = 0; i < maxPopulation; i++) {
			newpopulation.add(population.get(i));
			//System.out.println("Fitness: " + population.get(i).getFitness());
		}
		population = newpopulation;
		
//		Collections.sort(population);
		System.out.println(population.get(0).getFitness());
	}
	
	
	public void doMating(){
//		Collections.shuffle(population, matingRand);
		Collections.sort(population);
		
		int populationSize = population.size();
		int qtdCrossing = 0;
		
		for (int i = 0; i < populationSize; i++) {			
			if(matingRand.nextDouble() < crossRate){
				qtdCrossing++;
			
				Individuo parentA = population.get(i);
				Individuo parentB = population.get(matingRand.nextInt((int)(populationSize * 0.1)));
				
				// Fuk Fuk, e crossover
				Individuo<?> childA = factory.createIndividuo(numDimensions, minLimit, maxLimit, doOnePointCrossover(parentA, parentB));
				Individuo<?> childB = factory.createIndividuo(numDimensions, minLimit, maxLimit, doOnePointCrossover(parentB, parentA));
				
				doMutation(childA);
				doMutation(childB);
				
				childA.setFitness(calculateFitness(childA));
				childB.setFitness(calculateFitness(childB));
							
				population.add(childA);
				population.add(childB);
			}
		}
		System.out.println("Quantidade de cruzamentos: " + qtdCrossing);
	}
	
	/**
	 * TODO: Permitir dividir em qualquer lugar do vetor?
	 */
	public <T> T[] doOnePointCrossover(Individuo<T> parentA, Individuo<T> parentB) {
		T[] child = (T[]) Array.newInstance(parentA.getData().getClass().getComponentType(), numDimensions);
		
		// Escolhe um ponto de corte
		int cut = matingRand.nextInt(numDimensions);
		//int cut = numDimensions/2;
		
		// Faz o crossover
		for(int i = 0; i < numDimensions; i++){
			child[i] = ( i < cut ? parentA.getData()[i] : parentB.getData()[i] );
		}
		
		return child;
	}
	
	public void doMutation(Individuo<?> individuo){ 
		for (int i = 0; i < individuo.getData().length; i++) {
			if(matingRand.nextDouble() < mutationRate){
				individuo.mutateGene(i);
			}
		}
	}
	
}
