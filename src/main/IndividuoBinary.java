package main;


public class IndividuoBinary extends Individuo<String> {
	private int qtdSteps;
	private double stepSize;
	private int qtdBinary;
	private Double[] doubleCache;

	public IndividuoBinary(int numDimensions, double minLimit, double maxLimit){
		super(numDimensions, minLimit, maxLimit);
		qtdBinary = 12;
		qtdSteps = (int) Math.pow(2, qtdBinary);
		stepSize = (maxLimit - minLimit) / ((double)(qtdSteps));
		doubleCache = new Double[numDimensions];
		
		chromossome = new String[this.numDimensions];
		fillChromossome();
	}
	
	public IndividuoBinary(int numDimensions, double minLimit, double maxLimit, String[] data){
		super(numDimensions, minLimit, maxLimit, data);
		qtdBinary = 12;
		qtdSteps = (int) Math.pow(2, qtdBinary);
		stepSize = (maxLimit - minLimit) / ((double)(qtdSteps));
		doubleCache = new Double[numDimensions];
	}

	protected void fillChromossome(){
		for(int i = 0; i < chromossome.length; i++){
			chromossome[i] = getNextBinaryRand();
		}
	}
	
	private String getNextBinaryRand(){
		String binary = "";
		for(int i = 0; i < qtdBinary; i++) binary += indRand.nextInt(2) == 1 ? "1" : "0";
		return binary;
	}
		
	public String flip(String bit){
		return bit.equals("1") ? "0" : "1";
	}
	
	public void mutateGene(int a){
		String genes = "";
		for(int i = 0; i < qtdBinary; i++) {
			String gene = chromossome[a].substring(i, i+1);
			genes += (indRand.nextInt(qtdBinary) < indRand.nextInt(qtdBinary)) ? flip(gene) : gene;
		}
		chromossome[a] = genes;
	}
	
	@Override
	public Double[] toDouble() {
		for(int i = 0; i < numDimensions; i++){
			doubleCache[i] = Integer.parseInt(chromossome[i], 2) * stepSize + minLimit;
			//System.out.println("Double Cache " + i + " : " + doubleCache[i]);
		}
		return doubleCache;
	}
}
