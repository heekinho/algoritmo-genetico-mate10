package main;

import java.util.Arrays;
import java.util.Random;


public abstract class Individuo<T> implements Comparable<Individuo<T>>  {	
	public static Random indRand = new Random(25556);
	
	protected int numDimensions;
	protected double minLimit, maxLimit;
	
	protected T[] chromossome;
	protected double fitness;
	
	public Individuo(int numDimensions, double minLimit, double maxLimit){
		this.numDimensions = numDimensions;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
	}
	
	public Individuo(int numDimensions, double minLimit, double maxLimit, T[] data){
		this.numDimensions = numDimensions;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
		chromossome = data;
	}

	public T[] getData() {
		return chromossome;
	}

	public void setData(T[] data) {
		this.chromossome = data;
	}
	
	/**
	 * NOTA: Resolvi modelar o fitness como um atributo ordinário.
	 * Quem realmente calcula e define o fitness do Individuo é a população
	 * @param fitness
	 */
	public void setFitness(double fitness) {
		this.fitness = fitness;
	}
	
	/**
	 * Obtém o fitness do indivíduo
	 * @return
	 */
	public double getFitness(){
		return fitness;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(chromossome);
	}


	@Override
	public int compareTo(Individuo<T> other) {
		if(this.fitness > other.fitness) return 1;
		else if(this.fitness < other.fitness) return -1;
		else return 0;
		
		//This one gives: Comparison method violates its general contract
		//return (int) Math.ceil(this.fitness - other.fitness);
	}
	
	protected abstract void fillChromossome();
	
	public abstract void mutateGene(int a);
	
	public abstract Double[] toDouble();
}
