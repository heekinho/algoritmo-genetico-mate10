package main;

public interface FitnessFunction {
	public double getFitness(Double[] x, Double[] base);
	//public double getFitness(boolean[][] x);
}
