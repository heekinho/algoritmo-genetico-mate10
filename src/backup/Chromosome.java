package backup;

import java.util.Random;


// Fazer tipo "MyBitSet" e extender...
public class Chromosome /* extends BitSet */ implements Comparable<Chromosome> {
	//private BitSet genes;
	private boolean[] genes;
	
	// Talvez, em POO n�o seja o melhor lugar... mas...
	private double fitness = 0;
	
	public double getFitness() { return fitness; }
	public void setFitness(double fitness) { this.fitness = fitness; }
	
	
	// Desisti de usar BitSet pq tava bugando...
	public Chromosome(int ngenes){
		genes = new boolean[ngenes];
		
		Random r = new Random();
		// Preenche o cromossomo aleatóriamente
		for (int i = 0; i < this.genes.length; i++) {
			this.genes[i] = r.nextBoolean();
		}
	}
	

	public Chromosome(String genes){
		this(genes.length());
		
		// A partir de uma string, inicializa o vetor
		for (int i = 0; i < this.genes.length; i++) {
			set(this.genes.length-1-i, (genes.charAt(i) == '1'));
		}
	}
	
	public void set(int index, boolean value){
		genes[index] = value;
	}
	
	public void set(int index, int value){
		genes[index] = (value > 0);
	}
	
	public void set(int index){
		genes[index] = true;
	}
	
	public boolean get(int index){
		return genes[index];
	}
	
	public String getGenes(){
		String gene = "";
		for (int i = 0; i < genes.length; i++) gene += get(i)?"1":"0";
		return gene;
	}

	
	public void flip(int index){
		if(genes[index]) genes[index] = false;
		else genes[index] = true;
	}
	
	
	private double getDoubleValue(){
		double value = 0;
		for (int i = 0; i < genes.length; i++) {
			value += (get(i)?1:0) * Math.pow(2, i);
		}
		return value;
	}

	
	public void print(){
		for (int i = 0; i < genes.length; i++) {
			System.out.println(this.get(i));
		}
	}

	// Deveria estar nesta classe?
	public double getRelativeDouble(double rawValue, double domainInf, double domainSize){
		return domainInf + (rawValue * 3)/(Math.pow(2, genes.length) - 1);
	}
	
	public double getX(double domainInf, double domainSize){
		//return getRelativeDouble(this.getDoubleValue(), domainInf, domainSize);
		return domainInf + (getDoubleValue() * 3)/(Math.pow(2, genes.length) - 1);
	}
	
	
	@Override
	public int compareTo(Chromosome o) {
		//return (int)Math.round(this.fitness - o.fitness);
		return (int)Math.ceil(this.fitness - o.fitness);
	}
}
