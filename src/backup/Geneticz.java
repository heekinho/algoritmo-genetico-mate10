package backup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Geneticz {
	private ArrayList<Chromosome> population;
	
	private int maxIndividuos;
	private int ngenes;
	private double taxacruzamento;
	private double taxamutacao;
	
	
	
	// Quantidade Max de individuos
	// Quantidade de genes de cada maluco
	public Geneticz(int maxIndividuos, int ngenes, double tcruzamento, double tmutacao, int espacoInicial){
		this.maxIndividuos = maxIndividuos;
		this.ngenes = ngenes;
		this.taxacruzamento = tcruzamento;
		this.taxamutacao = tmutacao / ngenes; // sobre ngenes mesmo?
		
		population = new ArrayList<Chromosome>();
		fillPopulation(espacoInicial);
		
		doEvolution();
	}
	
	// Alfa: multiplica o tamanho da geracao inicial.
	// Pois amplia o espa�o de busca e talz...
	public void fillPopulation(int alfa){
		//population.clear();
		for (int i = 0; i < maxIndividuos * alfa; i++) {
			//Chromosome c = new Chromosome(ngenes);
			//c.setFitness(fitnessFunction(c));
			population.add(new Chromosome(ngenes));
		}
	}
	
	
	public void doEvolution(){ //evolve
		// Inicializado jah...
		doEvaluation();
		//boolean flag = false;
		for (int t = 0; t < 1000; t++) {
			//System.out.println(t);
			doNaturalSelection();
			doMating();
			doEvaluation();
		}
		Collections.sort(population);
		System.out.println(population.get(0).getX(-1, 3) + " - " + population.get(0).getFitness());
	}
	
	
	public void doEvaluation(){
		// Aproveito para atualizar o fitness total
		// N�o quis propagar por metodos... fica com campo
		totalFitness = 0;
		for (Chromosome c : population) {
			c.setFitness(fitnessFunction(c));
			totalFitness += c.getFitness();
		}
	}
	
	public void doNaturalSelection(){
		//doRoleta();
		
		Collections.sort(population);
		ArrayList<Chromosome> newpopulation = new ArrayList<Chromosome>();
		for (int i = 0; i < maxIndividuos; i++) {
			newpopulation.add(population.get(i));
		}
		population = newpopulation;
		
		//Collections.sort(population);
		System.out.println(population.get(0).getFitness());
	}
	
	
	public void doMating(){
		//Collections.shuffle(population);
		
		for (int i = 0; i < maxIndividuos; i++) {
			Chromosome c = population.get(i);
			
			if(Math.random() < taxacruzamento){
				// Cruzamento propriamente dito
				String pai = c.getGenes();
				String mae = population.get((new Random().nextInt(maxIndividuos))).getGenes();
				
				// Fuk Fuk, e crossover
				Chromosome filhoa = new Chromosome(pai.substring(0, ngenes/2) + mae.substring(ngenes/2, ngenes));
				doMutation(filhoa);
				Chromosome filhob = new Chromosome(mae.substring(0, ngenes/2) + pai.substring(ngenes/2, ngenes));
				doMutation(filhob);
			
				population.add(filhoa);
				population.add(filhob);
				
//				population.add(new Chromosome(pai.substring(0, ngenes/2) + mae.substring(ngenes/2, ngenes)));
//				doMutation(population.get(population.size()-1));
//				population.add(new Chromosome(mae.substring(0, ngenes/2) + pai.substring(ngenes/2, ngenes)));
//				doMutation(population.get(population.size()-1));
			}
		}
	}
	private double totalFitness = 0;
	
	
	public void doMutation(Chromosome c){ //mutate
		for (int i = 0; i < ngenes; i++) {
			if(Math.random() < taxamutacao){
				c.flip(i);
			}
		}
	}
	
	public void doRoleta(){
		// Nova popula��o
		ArrayList<Chromosome> newpopulation = new ArrayList<Chromosome>();
		
		// Seleciona os n individuos da popula��o
		for (int i = 0; i < maxIndividuos; i++) {
			//double r = Math.random() * t;
			double r = Math.random() * totalFitness;
			
			double s = 0;
			for (Chromosome c : population) {
				s += c.getFitness();
				
				if (s >= r) {
					// TODO: Verificar se precisa verificar se j� existe
					// Alteraria algo por akew
					newpopulation.add(c);
					break;
				}
			}
		}
		
		// Nova populacao pronta
		population = newpopulation;
	}
	
	

	public double objectiveFunction(double x){
		return (x * Math.sin(10*Math.PI*x)) + 1;
	}
	
	public double fitnessFunction(Chromosome c){
		//return objectiveFunction(c.getRelativeDouble(c.getDoubleValue(), -1.0, 3.0));
		return objectiveFunction(c.getX(-1.0, 3.0));
	}
	
	
}
